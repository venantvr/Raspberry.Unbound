import re
import os
import pycurl

directory = '/tmp/dnsblock/'

if not os.path.exists(directory):
	os.makedirs(directory)

i = 0
domains = []
files = []

url = 'https://www.hack-my-domain.fr/wp-content/uploads/free-tools/hosts.txt'

c = pycurl.Curl()
c.setopt(c.URL, url)
target = directory + 'sources.txt'

with open(target, 'w') as f:
        c.setopt(c.WRITEFUNCTION, f.write)
        c.perform()

with open(target) as f:
        for line in f:
                item = line.strip()
                match = len(item) > 0 and item.startswith('http')
                if match:
                        files.append(item)

for file in files:
	c = pycurl.Curl()
	c.setopt(c.URL, file)
	target = directory + str(i) + '.txt'

	# print file + ' -> ' + target

	with open(target, 'w') as f:
		c.setopt(c.WRITEFUNCTION, f.write)
		c.perform()

	with open(target) as f:
		for line in f:
			item = line.strip()
			match = re.match(r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}[ \t]+[a-zA-Z\d-]{,63}(\.[a-zA-Z\d-]{,63})+$', item)
			if match:
				item = line.split()[-1]
				correct = re.match(r'^[a-zA-Z\d-]{,63}(\.[a-zA-Z\d-]{,63})*$', item)
				if correct:
					domains.append(item)
	i = i + 1

filtered = dict.fromkeys(domains).keys()

with open('/etc/unbound/includes/malwaredomainlist-blocking.conf', 'w') as file:
	for domain in filtered:
		file.write('local-zone: \"%s\" static\n' % domain)

