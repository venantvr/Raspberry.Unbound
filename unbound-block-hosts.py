import re
for line in open('/tmp/dnsblock/dnsblock.txt', 'r'):
	item = line.split()[-1]
	# print(item)
	correct = re.match(r'^[a-zA-Z\d-]{,63}(\.[a-zA-Z\d-]{,63})*$', item)
	# if '>' not in line:
	if correct:
		print('local-zone: \"%s\" static' % item)
